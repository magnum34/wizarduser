﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WizardUser
{
    public class WindowsStep
    {

        private Users user;
        public static int StepCounter = 0;
        private MainWindow window;
        public bool buttonNext = false;
        public WindowsStep(MainWindow window)
        {
            Context context = new Context();
            user = new Users();
            this.window = window;
            window.lbData.Content = Context.labelText[0]; 
            window.btnPrevious.Visibility = System.Windows.Visibility.Hidden;
            if (String.IsNullOrEmpty(user.first_name))
            {
                user.first_name = window.text.Text;
            }
            else
            {
                window.text.Text = user.first_name;
            }
        }
        public WindowsStep(MainWindow window,ref Users user,int StepCounter)
        {
            this.window = window;
            this.user = user;
            Context context = new Context();
            WindowsStep.StepCounter = StepCounter;
            ReadData();

        }
        public void  saveData()
        {
            if (StepCounter < 0)
            {
                StepCounter = 0;
            }
            else if (StepCounter > 3)
            {
                StepCounter = 3;
            }
            switch (StepCounter)
            {
                case 0:
                    window.lbData.Content = Context.labelText[0];
                    window.btnPrevious.Visibility = System.Windows.Visibility.Hidden;
                    if (String.IsNullOrEmpty(user.first_name) )
                    {
                        user.first_name = window.text.Text;
                    }else if (user.first_name != window.text.Text)
                    {
                        user.first_name = window.text.Text;
                    }
                    else
                    {
                        window.text.Text = user.first_name;
                    }
                    break;
                case 1:
                    window.btnPrevious.Visibility = System.Windows.Visibility.Visible;
                    window.lbData.Content = Context.labelText[1];
                    if (String.IsNullOrEmpty(user.last_name))
                    {
                        user.last_name = window.text.Text;
                    }
                    else if (user.last_name != window.text.Text)
                    {
                        user.last_name = window.text.Text;
                    }
                    else
                    {
                        window.text.Text = user.last_name;
                    }
                    break;
                case 2:
                    window.lbData.Content = Context.labelText[2];
                    if (String.IsNullOrEmpty(user.address) )
                    {
                        user.address = window.text.Text;
                    }
                    else if (user.address != window.text.Text)
                    {
                        user.address = window.text.Text;
                    }
                    else
                    {
                        window.text.Text = user.address;
                    }
                    break;
                case 3:
                    window.lbData.Content = Context.labelText[3];
                    if (String.IsNullOrEmpty(user.phone) )
                    {
                        user.phone = window.text.Text;
                    }
                    else if (user.phone != window.text.Text)
                    {
                        user.phone = window.text.Text;
                    }
                    else
                    {
                        window.text.Text = user.address;
                    }
                    break;
            }
            window.text.Text = "";
        }
        public void ReadData()
        {
            if (StepCounter < 0)
            {
                StepCounter = 0;
            }
            else if (StepCounter > 4)
            {
                StepCounter = 4;
            }
            switch (StepCounter)
            {
                case 0:
                    window.btnPrevious.Visibility = System.Windows.Visibility.Hidden;
                    window.lbData.Content = Context.labelText[0];
                    window.text.Text = user.first_name;
                    break;
                case 1:
                    window.btnPrevious.Visibility = System.Windows.Visibility.Visible;
                    window.lbData.Content = Context.labelText[1];
                    window.text.Text = user.last_name;         
                    break;
                case 2:
                    window.lbData.Content = Context.labelText[2];

                    window.text.Text = user.address;
                    break;
                case 3:
                    window.lbData.Content = Context.labelText[3];
                    window.text.Text = user.phone;
                    break;
                case 4:
                    DetailWindow detail = new DetailWindow(ref user);
                    window.Close();
                    detail.Show();
                    break;
            }
            
        }

    }
}

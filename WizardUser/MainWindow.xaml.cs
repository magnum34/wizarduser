﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WizardUser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
 
    public partial class MainWindow : Window
    {
        private WindowsStep step;
        private Context context;
        public MainWindow()
        {
            InitializeComponent();
            context = new Context();
            btnNext.Content = Context.labelText[4];
            btnPrevious.Content = Context.labelText[5];
            text.Text = "";
            MainWindow parentWindow = (MainWindow)Window.GetWindow(this);
            step = new WindowsStep(parentWindow);

        }
        public MainWindow(ref Users user,int StepCounter)
        {
            InitializeComponent();
            context = new Context();
            btnNext.Content = Context.labelText[4];
            btnPrevious.Content = Context.labelText[5];
            MainWindow parentWindow = (MainWindow)Window.GetWindow(this);
            step = new WindowsStep(parentWindow,ref user,StepCounter);

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(text.Text))
            {
                step.saveData();
                WindowsStep.StepCounter++;
                step.ReadData();
            }
            else
            {
                System.Windows.MessageBox.Show(Context.labelText[6],"Required",MessageBoxButton.OK,MessageBoxImage.Information);
            }

        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            WindowsStep.StepCounter--;
            step.ReadData();
        }

        private void text_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WizardUser
{
    public class Context
    {
        public static List<string> labelText = new List<string>();
        public Context()
        {
            labelText.Add("First Name :");
            labelText.Add("Last Name : ");
            labelText.Add("Address : ");
            labelText.Add("Phone : ");
            labelText.Add("Next");
            labelText.Add("Previous");
            labelText.Add("Required field");
        }
    }
}

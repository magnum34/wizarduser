﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WizardUser
{
    /// <summary>
    /// Interaction logic for DetailWindow.xaml
    /// </summary>
    public partial class DetailWindow : Window
    {
        private Users user;

        public DetailWindow()
        {
            InitializeComponent();
            Context context = new Context();
            lbFirstName.Content = Context.labelText[0];
            lbLastName.Content = Context.labelText[1];
            lbAddress.Content = Context.labelText[2];
            lbPhone.Content = Context.labelText[3];
            btnPrevious.Content = Context.labelText[5];


        }
        public DetailWindow(ref Users  user)
        {
            this.user = user;
            InitializeComponent();
            Context context = new Context();
            lbFirstName.Content = Context.labelText[0];
            lbLastName.Content = Context.labelText[1];
            lbAddress.Content = Context.labelText[2];
            lbPhone.Content = Context.labelText[3];
            btnPrevious.Content = Context.labelText[5];
            lbFirstNameInput.Content = user.first_name;
            lbLastNameInput.Content = user.last_name;
            lbAddressInput.Content = user.address;
            lbPhoneInput.Content = user.phone;
        }


        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            
            MainWindow main = new MainWindow(ref user,3);
            main.Show();
            this.Close();

        }
    }
}
